python manage.py makemigrations Snippets
python manage.py migrate

env\Scripts\activate

python manage.py shell
python manage.py runserver

rm -f db.sqlite3
rm -r snippets/migrations
python manage.py makemigrations snippets
python manage.py migrate

python manage.py createsuperuser
user
user@example.com
useradmin

user1
use1@example.com
user1admin

pip install coreapi

urls.py:
    from rest_framework.schemas import get_schema_view

    schema_view = get_schema_view(title='Pastebin API')

    urlpatterns = [
        url(r'^schema/$', schema_view),
        ...
    ]

    python manage.py runserver
    http://127.0.0.1:8000/schema/

#Using a command line client:
pip install coreapi-cli
coreapi
coreapi get http://127.0.0.1:8000/schema/
    <Pastebin API "http://127.0.0.1:8000/schema/">
        snippets: {
            list([page])
            read(id)
            highlight(id)
        }
        users: {
            list([page])
            read(id)
        }

coreapi action snippets list

#Authenticating our client:
coreapi credentials add 127.0.0.1 <username>:<password> --auth basic
    Added credentials
    127.0.0.1 "Basic ..."

coreapi reload

coreapi action snippets create --param title="Example" --param code="print('hello, world')"